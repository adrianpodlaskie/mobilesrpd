package com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.malz.adrian.mobilesrpd.R;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.models.Thesis;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class ThesesListAdapter extends ArrayAdapter<Thesis> implements Filterable {
    private final Activity context;
    private List<Thesis> thesesList;
    private List<Thesis> filteredThesis = null;
    private CustomFilter filter = new CustomFilter();


    public ThesesListAdapter(Activity context, List<Thesis> thesesList) {

        super(context, R.layout.thesis_item, thesesList);

        this.context = context;
        this.thesesList = thesesList;
        this.filteredThesis = thesesList;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.thesis_item, null,true);

        TextView txtTitle = rowView.findViewById(R.id.item);
        TextView idThesis = rowView.findViewById(R.id.idThesisTextView);
        TextView owner = rowView.findViewById(R.id.ownerNameTextView);

        idThesis.setText("id: " + thesesList.get(position).getId().toString());
        owner.setText(filteredThesis.get(position).getOwner());

        ImageView imageView = rowView.findViewById(R.id.icon);

        txtTitle.setText(filteredThesis.get(position).getTopic());

        TextView ownerSurnameFirstLetter = rowView.findViewById(R.id.firstLetterOfOwnerTextView);
        ownerSurnameFirstLetter.setText(getFirstLetterOfSurname(filteredThesis.get(position).getOwner()));

        int tmp = position % 3;
        if(tmp == 0)
             imageView.setImageResource(R.drawable.circle_purple);
        else if(tmp == 1)
            imageView.setImageResource(R.drawable.circle_green);
        else imageView.setImageResource(R.drawable.circle_blue);

        rowView.setBackgroundResource(position % 2 == 0 ? R.drawable.list_view_background_white : R.drawable.list_view_background);


        return rowView;

    }

    public int getCount() {
        return filteredThesis.size();
    }

    public Thesis getItem(int position) {
        return filteredThesis.get(position);
    }

    public Filter getFilter() {
        return filter;
    }

    private String getFirstLetterOfSurname(String surname) {
        for(int i = surname.length() - 1; i > 0; i--) {
            if(surname.charAt(i-1) ==  ' ')
                return Character.toString(surname.charAt(i));
        }
        return "A";
    }

    private class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String filterString = charSequence.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Thesis> list = thesesList;

            int count = list.size();
            final ArrayList<Thesis> newFilteredThesis = new ArrayList<Thesis>(count);

            Thesis filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i);
                if (filterableString.getTopic().toLowerCase().contains(filterString)) {
                    newFilteredThesis.add(filterableString);
                }
            }

            results.values = newFilteredThesis;
            results.count = newFilteredThesis.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            filteredThesis = (ArrayList<Thesis>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}
