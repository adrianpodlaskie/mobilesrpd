package com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.services;

import com.malz.adrian.mobilesrpd.OkhttpClientManager;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.ThesisAction;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.interfaces.ThesisDetailsActionCallback;
import okhttp3.*;

import java.io.IOException;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class SignedThesisService {
    private static final String SUBSCRIBE_THESIS_BY_ID = "https://srpd.wi.pb.edu.pl/student/thesis/choice/";
    private static final String UNSUBSCRIBE_THESIS_BY_ID = "https://srpd.wi.pb.edu.pl/student/thesis/withdraw/";


    private OkHttpClient client = OkhttpClientManager.getOkhttpClientInstance();
    private com.malz.adrian.mobilesrpd.OkHttp3CookieHelper cookieHelper = OkhttpClientManager.getCookieHelperInstance();


    private static final SignedThesisService instance = new SignedThesisService();

    private SignedThesisService(){}

    public static SignedThesisService getInstance(){
        return instance;
    }

    private void prepareForRequest(String url) {
        cookieHelper.setCookie(url, "cookie_name", "cookie_value");
    }

    public void signToThesisById(String id, final ThesisDetailsActionCallback thesisDetailsActionCallback) {

        prepareForRequest(SUBSCRIBE_THESIS_BY_ID + id + OkhttpClientManager.IS_MOBILE);

        okhttp3.Request authTokenRequest = new Request.Builder()
                .url(SUBSCRIBE_THESIS_BY_ID + id + OkhttpClientManager.IS_MOBILE)
                .get()
                .addHeader("Referer", OkhttpClientManager.REFERER)
                .addHeader("Content-Type", OkhttpClientManager.CONTENT_TYPE)
                .build();

        client.newCall(authTokenRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                thesisDetailsActionCallback.onFailure(ThesisAction.SIGN);
            }

            @Override
            public void onResponse(Call call, Response response) {
                thesisDetailsActionCallback.onSuccess(ThesisAction.SIGN);
            }
        });
    }

    public void unSignThesisById(String id, ThesisDetailsActionCallback thesisDetailsActionCallback) {
        prepareForRequest(UNSUBSCRIBE_THESIS_BY_ID + id + OkhttpClientManager.IS_MOBILE);

        okhttp3.Request authTokenRequest = new Request.Builder()
                .url(UNSUBSCRIBE_THESIS_BY_ID + id + OkhttpClientManager.IS_MOBILE)
                .get()
                .addHeader("Referer", OkhttpClientManager.REFERER)
                .addHeader("Content-Type", OkhttpClientManager.CONTENT_TYPE)
                .build();

        client.newCall(authTokenRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                thesisDetailsActionCallback.onFailure(ThesisAction.SIGNOFF);
            }

            @Override
            public void onResponse(Call call, Response response) {
                thesisDetailsActionCallback.onSuccess(ThesisAction.SIGNOFF);
            }
        });
    }
}
