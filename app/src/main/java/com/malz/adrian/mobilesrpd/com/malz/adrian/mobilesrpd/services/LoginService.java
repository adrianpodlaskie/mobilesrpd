package com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.services;

import android.util.Log;
import com.malz.adrian.mobilesrpd.*;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.interfaces.LoginCallback;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class LoginService {
    private static final String LOGIN_URL = "https://srpd.wi.pb.edu.pl/login?is_mobile=true";

    private static final Integer INDEX_OF_FIRST_TOKEN_CHAR = 10;
    private static final Integer INDEX_OF_LAST_TOKEN_CHAR = 42;

    private static final String LOGIN_ERROR_USER_PASS = "niepoprawne";


    private OkHttp3CookieHelper cookieHelper = OkhttpClientManager.getCookieHelperInstance();
    private OkHttpClient client = OkhttpClientManager.getOkhttpClientInstance();

    public String auth_token;
    private LoginCallback loginCallback;

    private static final LoginService instance = new LoginService();

    private LoginService(){}

    public static LoginService getInstance() {return instance;}


    public void login(String username, String pass, LoginCallback loginCallback) {
        this.loginCallback = loginCallback;
        prepareForRequest();
       getAuthToken(username, pass);
    }

    private void prepareForRequest() {
        cookieHelper.setCookie(LOGIN_URL, "cookie_name", "cookie_value");
    }


    private void getAuthToken(String username, String pass) {
        okhttp3.Request authTokenRequest = new Request.Builder()
                .url(LOGIN_URL)
                .get()
                .addHeader("Referer", OkhttpClientManager.REFERER)
                .addHeader("Content-Type", OkhttpClientManager.CONTENT_TYPE)
                .build();

        client.newCall(authTokenRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                loginCallback.onFailure("Logowanie nieudane. Sprawdź połączenie.");
            }

            @Override
            public void onResponse(Call call, Response response)   {
                if (auth_token == null) {
                    auth_token = response.headers().value(3).substring(INDEX_OF_FIRST_TOKEN_CHAR, INDEX_OF_LAST_TOKEN_CHAR);
                    System.out.println(auth_token);
                } else System.out.println("auth token null");

                loginIntoSrpd(username, pass);
            }
        });
    }

    private void loginIntoSrpd(String username, String pass) {
        RequestBody formBody;
        formBody = new FormBody.Builder()
                .add("csrfmiddlewaretoken", auth_token)
                .add("username", username)
                .add("password", pass)
                .build();
        okhttp3.Request request1 = new Request.Builder()
                .url(LOGIN_URL)
                .post(formBody)
                .addHeader("Referer", OkhttpClientManager.REFERER)
                .addHeader("Content-Type", OkhttpClientManager.CONTENT_TYPE)
                .build();

        client.newCall(request1).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
              loginCallback.onFailure("Logowanie nieudane. Sprawdź połączenie.");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String full_name = response.body().string();
                if (full_name.contains(LOGIN_ERROR_USER_PASS)) {
                    loginCallback.onFailure("Nieprawidłowa nazwa użytkownika lub hasło!");
                }
                else {
                    loginCallback.onSuccess("Logowanie udane", full_name);
                }
            }
        });
    }
}
