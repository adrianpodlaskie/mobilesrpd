package com.malz.adrian.mobilesrpd;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.adapters.ThesesListAdapter;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.ModeOfDetailsView;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.TypesOfThesis;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.interfaces.ThesisCallback;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.models.Thesis;

import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.services.ThesisService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class ThesisListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ThesisCallback {

    private static String TAG = "ThesisList";

    ProgressDialog progressDialog;

    List<Thesis> allThesesList;
    ListView thesisListView;
    ThesesListAdapter adapter;
    SearchView searchView;
    String nameLetters;
    NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thesis_list);

        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        saveStudentName();
        setStudentNameInMenuHeader();

        initializeControls();
        registerSearchListener();
        subscribeToNotification();

        getThesis();


        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }
    }

    private void getThesis() {
        progressDialog.show();
        ThesisService.getInstance().getThesis(this);
    }


    private void initializeAdapter() throws InterruptedException {
        Log.d(TAG , "Start initializing adapter");
        adapter = new ThesesListAdapter(this, allThesesList);
        thesisListView.setAdapter(adapter);
        setOnClickAction();
    }

    private void setOnClickAction() {
        thesisListView.setOnItemClickListener((adapterView, view, position, l) -> {
            Intent goToDetailsIntent = new Intent(ThesisListActivity.this, ThesisDetails.class);
            goToDetailsIntent.putExtra("thesis", allThesesList.get(position));
            goToDetailsIntent.putExtra("MODE", ModeOfDetailsView.STANDARD);
            startActivity(goToDetailsIntent);
        });
    }

    private void initializeControls() {
        progressDialog = new ProgressDialog(ThesisListActivity.this);
        thesisListView = findViewById(android.R.id.list);
        progressDialog.setMessage(getResources().getString(R.string.download_theses));
        searchView = findViewById(R.id.searchView);
    }

    private void registerSearchListener() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String text) {
                adapter.getFilter().filter(text);

                return false;
            }
        });
    }

    private void subscribeToNotification() {
        FirebaseMessaging.getInstance().subscribeToTopic("all")
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Toast.makeText(ThesisListActivity.this, "Not Subscribed!", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        String menuItemItem = (String) menuItem.getTitle();
        switch (menuItemItem){
            case "Moje prace": {
                Intent goToListActivity = new Intent(ThesisListActivity.this, UserAccountActivity.class);
                goToListActivity.putExtra("TYPE_OF_LIST", TypesOfThesis.SIGNED_ACCEPTED);
                startActivity(goToListActivity);
                break;
            }
            case "Zaobserwowane prace": {
                Intent goToListActivity = new Intent(ThesisListActivity.this, UserAccountActivity.class);
                goToListActivity.putExtra("TYPE_OF_LIST", TypesOfThesis.OBSERVED);
                startActivity(goToListActivity);
                break;
            }
            case "Zapisane prace": {
                Intent goToListActivity = new Intent(ThesisListActivity.this, UserAccountActivity.class);
                goToListActivity.putExtra("TYPE_OF_LIST", TypesOfThesis.SIGNED);
                startActivity(goToListActivity);
                break;
            }
            case "O aplikacji": {
                Intent goToAboutActivity = new Intent(ThesisListActivity.this, AboutApplication.class);
                startActivity(goToAboutActivity);
                break;
            }
        }

        return false;
    }

    @Override
    public void onSuccess(List<Thesis> list) {
        progressDialog.dismiss();
        runOnUiThread(() -> {
            try {
                this.allThesesList = list;
                initializeAdapter();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        });
    }

    @Override
    public void onFailure(String action) {
        this.runOnUiThread(() -> {
            progressDialog.dismiss();
            Toast.makeText(ThesisListActivity.this, "Sprawdź połączenie!", Toast.LENGTH_LONG).show();
        });
    }

    @Override
    public void onBackPressed() {
        //blocked back to login activity
    }

    private String getFirstLetters(String name) {
        String[] array = name.split(" ");
        String firstLetter =  array[0].substring(0, 1);
        String secondLetter = array[1].substring(0, 1);

        return firstLetter + secondLetter;

    }

    private void setStudentNameInMenuHeader() {
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        nameLetters = sharedPref.getString("student_name", "");
        TextView nameTextView = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.nameTextView);
        nameTextView.setText(nameLetters);
        TextView nameLettersTextView = mNavigationView.getHeaderView(0).findViewById(R.id.firstLetterOfOwnerTextView);
        nameLettersTextView.setText(getFirstLetters(nameLetters));
    }

    private void saveStudentName() {
        if(getIntent().getSerializableExtra("student_name") != null) {
            String name = getIntent().getSerializableExtra("student_name").toString();
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("student_name", name);
            editor.commit();
        }
    }
}
