package com.malz.adrian.mobilesrpd;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.malz.adrian.mobilesrpd.api.model.User;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.adapters.ThesesListAdapter;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.ModeOfDetailsView;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.TypesOfThesis;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.interfaces.UserThesisCallback;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.models.Thesis;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.services.UserThesisService;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class UserAccountActivity extends AppCompatActivity implements UserThesisCallback {
    private  final String MY_SIGNED_ACCEPTED_THESIS_URL = "https://srpd.wi.pb.edu.pl/student/thesis/assigned?is_mobile=true";
    private  final String MY_OBSERVED_THESIS_URL = "https://srpd.wi.pb.edu.pl/student/thesis/observed?is_mobile=true";
    private  final String MY_SIGNED_THESIS_URL = "https://srpd.wi.pb.edu.pl/student/thesis/chosen?is_mobile=true";
    private static String TAG = "ThesisList";

    TextView headerTextView;
    ProgressDialog progressDialog;
    List<Thesis> allThesesList;
    ListView thesisListView;
    ThesesListAdapter adapter;

    TypesOfThesis type;
    private UserThesisService userThesisService = UserThesisService.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);

        initializeControls();
        type = (TypesOfThesis) getIntent().getSerializableExtra("TYPE_OF_LIST");
        updateHeaderLabel(type);

        getThesisByType(type);
    }

    private void getThesisByType(TypesOfThesis type) {
        progressDialog.show();
        if (type == TypesOfThesis.SIGNED_ACCEPTED) {
            userThesisService.getThesis(MY_SIGNED_ACCEPTED_THESIS_URL, this);
        } else if (type == TypesOfThesis.OBSERVED) {
            userThesisService.getThesis(MY_OBSERVED_THESIS_URL, this);
        } else if (type == TypesOfThesis.SIGNED) {
            userThesisService.getThesis(MY_SIGNED_THESIS_URL, this);
        }
    }

    public void initializeControls() {
        progressDialog = new ProgressDialog(UserAccountActivity.this);
        thesisListView = findViewById(android.R.id.list);
        progressDialog.setMessage(getResources().getString(R.string.download_theses));
        headerTextView = (TextView) findViewById(R.id.summaryTextView);
    }

    private void updateHeaderLabel(TypesOfThesis type) {
        if (type == TypesOfThesis.SIGNED_ACCEPTED) {
            headerTextView.setText("Moje prace:");
        } else if (type == TypesOfThesis.SIGNED) {
            headerTextView.setText("Zapisane prace:");
        } else if (type == TypesOfThesis.OBSERVED) {
            headerTextView.setText("Obserwowane prace:");
        }
    }

    private void initializeAdapter() throws InterruptedException {
        Log.d(TAG, "Start initializing adapter");
        adapter = new ThesesListAdapter(this, allThesesList);
        thesisListView.setAdapter(adapter);
        setOnClickAction();
    }

    private void setOnClickAction() {
        thesisListView.setOnItemClickListener((adapterView, view, position, l) -> {
            Intent goToDetailsIntent = new Intent(UserAccountActivity.this, ThesisDetails.class);
            goToDetailsIntent.putExtra("thesis", allThesesList.get(position));
            if (type == TypesOfThesis.SIGNED)
                goToDetailsIntent.putExtra("MODE", ModeOfDetailsView.SIGNED);
            else if (type == TypesOfThesis.OBSERVED)
                goToDetailsIntent.putExtra("MODE", ModeOfDetailsView.OBSERVED);

            startActivity(goToDetailsIntent);
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserAccountActivity.this, ThesisListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSuccess(List<Thesis> list) {
        runOnUiThread(() -> {
            progressDialog.dismiss();
            allThesesList = list;
            try {
                initializeAdapter();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onFailure(String msgS) {
        runOnUiThread(() -> {
           progressDialog.dismiss();
            Toast.makeText(UserAccountActivity.this, "Sprawdź połączenie!", Toast.LENGTH_SHORT).show();
        });
    }
}
