package com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.services;

import android.util.Log;
import com.malz.adrian.mobilesrpd.OkhttpClientManager;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.interfaces.UserThesisCallback;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.models.Thesis;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class UserThesisService {
    private OkHttpClient client = OkhttpClientManager.getOkhttpClientInstance();
    private com.malz.adrian.mobilesrpd.OkHttp3CookieHelper cookieHelper = OkhttpClientManager.getCookieHelperInstance();

    private static String TAG = "ThesisList";

    private static final UserThesisService instance = new UserThesisService();
    private List<Thesis> allThesesList;


    public UserThesisService() {}

    public static UserThesisService getInstance() {return instance;}

    public void getThesis(String url, UserThesisCallback callback) {
        prepareForRequest(url);

        Log.d(TAG, "Start getting theses");
        okhttp3.Request authTokenRequest = new Request.Builder()
                .url(url)
                .get()
                .addHeader("Referer", OkhttpClientManager.REFERER)
                .addHeader("Content-Type", OkhttpClientManager.CONTENT_TYPE)
                .build();

        client.newCall(authTokenRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
               callback.onFailure("Sprawdz połączenie!");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(TAG, "Get theses success response");
                String jsonData = response.body().string();
                JSONObject Jobject = null;
                JSONArray Jarray = null;
                allThesesList = new ArrayList<>();
                try {
                    Jobject = new JSONObject(jsonData);
                    Jarray = Jobject.getJSONArray("theses");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                /**
                 * Try to add new thesis
                 */
                for (int i = 0; i < Jarray.length(); i++) {
                    try {
                        JSONObject object = Jarray.getJSONObject(i);
                        Thesis thesis = Thesis.deserializeFromJson(object);
                        allThesesList.add(thesis);
                        Log.d(TAG, "Added new thesis to list");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.d(TAG, "All theses added");
                callback.onSuccess(allThesesList);

            }
        });
    }

    private void prepareForRequest(String url) {
        cookieHelper.setCookie(url, "cookie_name", "cookie_value");
    }
}
