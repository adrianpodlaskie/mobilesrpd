package com.malz.adrian.mobilesrpd;

import okhttp3.OkHttpClient;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class OkhttpClientManager {
    private static  OkHttpClient okhttpClientInstance = null;
    private static  OkHttp3CookieHelper cookieHelperInstance = null;

    public static final String IS_MOBILE = "?is_mobile=true";

    public static final String REFERER = "https://srpd.wi.pb.edu.pl/student/panel";
    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";

    protected OkhttpClientManager() {
    }

    public static OkHttpClient getOkhttpClientInstance() {
        if(okhttpClientInstance == null) {
            okhttpClientInstance =  new OkHttpClient.Builder().cookieJar(cookieHelperInstance.cookieJar()).build();
        }
        return okhttpClientInstance;
    }

    public static OkHttp3CookieHelper getCookieHelperInstance() {
        if(cookieHelperInstance == null) {
            cookieHelperInstance = new OkHttp3CookieHelper();
        }
        return cookieHelperInstance;
    }
}
