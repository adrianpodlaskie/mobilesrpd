package com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.interfaces;

import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.ThesisAction;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.TypesOfThesis;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public interface ThesisDetailsActionCallback {
     void onSuccess(ThesisAction action);
     void onFailure(ThesisAction action);
}
