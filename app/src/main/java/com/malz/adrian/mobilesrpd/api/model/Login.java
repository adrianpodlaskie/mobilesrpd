package com.malz.adrian.mobilesrpd.api.model;

public class Login {
    String username;
    String password;
    String csrfmiddlewaretoken;


    public Login(String username, String password, String csrfmiddlewaretoken) {
        this.username = username;
        this.password = password;
        this.csrfmiddlewaretoken = csrfmiddlewaretoken;
    }
}
