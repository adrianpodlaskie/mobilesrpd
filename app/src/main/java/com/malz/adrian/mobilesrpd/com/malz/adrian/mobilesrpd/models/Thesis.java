package com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Thesis implements Serializable {
    /** Topic of thesis */
    private String topic;
    /** List of scopes */
    private ArrayList<Scope> scopes;
    private Boolean dedicated;
    /** Description of thesis */
    private String description;
    /** Students assigned for this thesis */
    private ArrayList<Student> assignedStudents;
    private String keywords;
    private String eng_topic;
    private Integer type;
    /** Owner of thesis **/
    private String owner;
    Integer id;

    public  int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Thesis.counter = counter;

    }
    public static int counter = 0;

    public static Thesis deserializeFromJson(JSONObject jsonObject) throws JSONException {
        Thesis thesis = new Thesis();

        thesis.id = jsonObject.getInt("id");
        thesis.topic = jsonObject.getString("topic");
        thesis.type = jsonObject.getInt("type");
        thesis.eng_topic = jsonObject.getString("eng_topic");
        thesis.keywords = jsonObject.getString("keywords");
        thesis.assignedStudents =  initializeStudentsList(jsonObject.getJSONArray("assigned"));
        thesis.description = jsonObject.getString("description");
        thesis.dedicated = jsonObject.getBoolean("dedicated");
        thesis.scopes = initializeScopesList(jsonObject.getJSONArray("scopes"));
        thesis.owner = jsonObject.getString("owner");

        return thesis;
    }

    private static ArrayList<Student> initializeStudentsList(JSONArray jsonArray) {
        ArrayList<Student> studentsList = new ArrayList<>();
        for(int i = 0; i < jsonArray.length(); i++) {
            try {
                studentsList.add(Student.deserializeFromJson(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return studentsList;
    }

    private static ArrayList<Scope> initializeScopesList(JSONArray jsonArray) {
        ArrayList<Scope> scopesList = new ArrayList<>();
        for(int i = 0; i < jsonArray.length(); i++) {
            try {
                scopesList.add(Scope.deserializeFromJson(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return scopesList;
    }

    public Thesis() {}

    public Thesis(String topic, ArrayList<Scope> scopes, boolean dedicated, String description,
                  ArrayList<Student> assignedStudents, String keywords, String eng_topic,
                  Integer type, Integer id, String owner) {
        this.topic = topic;
        this.scopes = scopes;
        this.dedicated = dedicated;
        this.description = description;
        this.assignedStudents = assignedStudents;
        this.keywords = keywords;
        this.eng_topic = eng_topic;
        this.type = type;
        this.id = id;
        this.owner = owner;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public ArrayList<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(ArrayList<Scope> scopes) {
        this.scopes = scopes;
    }

    public boolean isDedicated() {
        return dedicated;
    }

    public void setDedicated(boolean dedicated) {
        this.dedicated = dedicated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Student> getAssignedStudents() {
        return assignedStudents;
    }

    public void setAssignedStudents(ArrayList<Student> assignedStudents) {
        this.assignedStudents = assignedStudents;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getEng_topic() {
        return eng_topic;
    }

    public void setEng_topic(String eng_topic) {
        this.eng_topic = eng_topic;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
