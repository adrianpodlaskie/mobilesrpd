package com.malz.adrian.mobilesrpd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.widget.Toast;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.ModeOfDetailsView;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.ThesisAction;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.enums.TypesOfThesis;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.interfaces.ThesisDetailsActionCallback;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.models.Scope;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.models.Student;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.models.Thesis;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.services.ObservedThesisService;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.services.SignedThesisService;

import java.util.List;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class ThesisDetails extends AppCompatActivity implements ThesisDetailsActionCallback {

    private Thesis tmpThesis;
    /**
     * Temat pracy
     **/
    private TextView topicTextView;

    /**
     * Zakres pracy
     **/
    private  TextView scopeTextView;

    /**
     * Typ pracy
     **/
    private TextView typeOfThesisTextView;

    /**
     * Rodzaj pracy
     **/
    private TextView typeSecondOfThesisTextView;

    /**
     * Assigned students
     **/
    private TextView assignedStudentTextView;

    private Button observeButton;
    private Button signInButton;
    private Button unobserveButton;
    private Button signoffButton;

    private ObservedThesisService observedThesisService = ObservedThesisService.getInstance();
    private SignedThesisService signedThesisService = SignedThesisService.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thesis_details);
        initializeControls();

        ModeOfDetailsView mode = (ModeOfDetailsView) getIntent().getSerializableExtra("MODE");
        setVisibilityOfButtons(mode);

        setContent();

        registerListeners();

    }

    private void setContent() {
        tmpThesis = (Thesis) getIntent().getSerializableExtra("thesis");
        topicTextView.setText(tmpThesis.getTopic());
        scopeTextView.setText(convertScopesListToString(tmpThesis.getScopes()));
        typeOfThesisTextView.setText(convertTypeToString(tmpThesis.getType()));
        typeSecondOfThesisTextView.setText(convertSecondTypeToString(tmpThesis.isDedicated()));
        assignedStudentTextView.setText(convertStudentsListToString(tmpThesis.getAssignedStudents()));
    }

    private void initializeControls() {
        topicTextView = findViewById(R.id.thesisTopicTextView);
        scopeTextView = findViewById(R.id.scopeTextView);
        typeOfThesisTextView = findViewById(R.id.typeOfThesisTextView);
        typeSecondOfThesisTextView = findViewById(R.id.typeOfThesisSecondTextView);
        assignedStudentTextView = findViewById(R.id.assignedStudentTextView);
        observeButton = findViewById(R.id.observeButton);
        signInButton = findViewById(R.id.signinButton);
        unobserveButton = findViewById(R.id.unobserveButton);
        signoffButton = findViewById(R.id.signOffButton);
    }

    private void setVisibilityOfButtons(ModeOfDetailsView mode) {
        if (mode == ModeOfDetailsView.OBSERVED) {
            observeButton.setVisibility(View.GONE);
            signInButton.setVisibility(View.GONE);
            unobserveButton.setVisibility(View.VISIBLE);
            signoffButton.setVisibility(View.GONE);
        } else if (mode == ModeOfDetailsView.SIGNED) {
            observeButton.setVisibility(View.GONE);
            signInButton.setVisibility(View.GONE);
            unobserveButton.setVisibility(View.GONE);
            signoffButton.setVisibility(View.VISIBLE);
        } else if (mode == ModeOfDetailsView.STANDARD) {
            observeButton.setVisibility(View.VISIBLE);
            signInButton.setVisibility(View.VISIBLE);
            unobserveButton.setVisibility(View.GONE);
            signoffButton.setVisibility(View.GONE);
        } else {
            observeButton.setVisibility(View.GONE);
            signInButton.setVisibility(View.GONE);
            unobserveButton.setVisibility(View.GONE);
            signoffButton.setVisibility(View.GONE);
        }
    }

    private void registerListeners() {
        observeButton.setOnClickListener(evt -> {
            String id = String.valueOf(tmpThesis.getId());
            observedThesisService.observeThesisById(id, this);
        });
        unobserveButton.setOnClickListener(evt -> {
            String id = String.valueOf(tmpThesis.getId());
            observedThesisService.unObserveThesisById(id, this);
        });
        signInButton.setOnClickListener(evt -> {
            String id = String.valueOf(tmpThesis.getId());
            signedThesisService.signToThesisById(id, this);
        });
        signoffButton.setOnClickListener(evt -> {
            String id = String.valueOf(tmpThesis.getId());
            signedThesisService.unSignThesisById(id, this);
        });
    }

    private String convertScopesListToString(List<Scope> scopes) {
        StringBuilder sb = new StringBuilder();
        for (Scope scope : scopes) {
            sb.append(scope.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    private String convertStudentsListToString(List<Student> students) {
        StringBuilder sb = new StringBuilder();
        for (Student student : students) {
            sb.append(student.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Change int from json object to type of thesis in string
     *
     * @param i int value from json
     * @return type of thesis
     */
    private String convertTypeToString(int i) {
        return (i == 0) ? getResources().getString(R.string.engineering_thesis) : getResources().getString(R.string.master_thesis);
    }

    private String convertSecondTypeToString(boolean dedicated) {
        return (dedicated) ? getResources().getString(R.string.individual) : getResources().getString(R.string.group);
    }

    @Override
    public void onSuccess(ThesisAction action) {
        this.runOnUiThread(() -> {
            if (action == ThesisAction.OBSERVE)
                Toast.makeText(this, R.string.successfullyobserved, Toast.LENGTH_LONG).show();
            else if (action == ThesisAction.UNOBSERVE) {
                Toast.makeText(this, R.string.successfullyUnObserved, Toast.LENGTH_LONG).show();
                Intent goToListActivity = new Intent(ThesisDetails.this, UserAccountActivity.class);
                goToListActivity.putExtra("TYPE_OF_LIST", TypesOfThesis.OBSERVED);
                startActivity(goToListActivity);
            } else if (action == ThesisAction.SIGN) {
                Toast.makeText(this, R.string.successfullysigned, Toast.LENGTH_LONG).show();
            } else if (action == ThesisAction.SIGNOFF) {
                Toast.makeText(this, R.string.successfullyunsigned, Toast.LENGTH_LONG).show();
                Intent goToListActivity = new Intent(ThesisDetails.this, UserAccountActivity.class);
                goToListActivity.putExtra("TYPE_OF_LIST", TypesOfThesis.SIGNED);
                startActivity(goToListActivity);
            }
        });

    }

    @Override
    public void onFailure(ThesisAction action) {
        this.runOnUiThread(() -> {
            if (action == ThesisAction.OBSERVE)
                Toast.makeText(this, R.string.failureobserved, Toast.LENGTH_LONG).show();
            else if (action == ThesisAction.UNOBSERVE) {
                Toast.makeText(this, R.string.failuteUnObserve, Toast.LENGTH_LONG).show();
            }   else if (action == ThesisAction.SIGN) {
                Toast.makeText(this, R.string.failuteUnObserve, Toast.LENGTH_LONG).show();
            }  else if (action == ThesisAction.SIGNOFF) {
                Toast.makeText(this, R.string.failuteUnObserve, Toast.LENGTH_LONG).show();
            }
        });
    }
}
