package com.malz.adrian.mobilesrpd;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.interfaces.LoginCallback;
import com.malz.adrian.mobilesrpd.com.malz.adrian.mobilesrpd.services.LoginService;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright (c) 2018-2019 Adrian Małż, mobilesrpd@gmail.com
 *
 *
 *     This file is part of MobileSRPD.
 *
 *     MobileSRPD is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MobileSRPD is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MobileSRPD.  If not, see <http://www.gnu.org/licenses/>.
 */
public class MainActivity extends AppCompatActivity implements LoginCallback {

    private Button loginButton;
    private EditText loginEditText;
    private EditText passwordEditText;

    LoginService loginService = LoginService.getInstance();

    /**
     * Progress dialog displayed during login
     */
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        changeThreadPolicy();

        initializeControls();
        setActionForControls();


    }

    private void initializeControls() {
        loginButton = (Button) findViewById(R.id.loginButton);
        loginEditText = findViewById(R.id.loginEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        progressDialog = new ProgressDialog(MainActivity.this);
    }

    private void setActionForControls() {
        loginButton.setOnClickListener(view -> {
         Toast.makeText(MainActivity.this, "WORKS!", Toast.LENGTH_LONG);

                progressDialog.setMessage(getResources().getString(R.string.dialog_message_login));
                progressDialog.show();
                loginService.login(loginEditText.getText().toString(), passwordEditText.getText().toString(), this);
        });
    }



    private void changeThreadPolicy() {
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }


    @Override
    public void onSuccess(String action, String msg) {
        this.runOnUiThread(() -> {
            progressDialog.dismiss();
            String studentName = getNameString(msg);
            Intent goToThesisList = new Intent(MainActivity.this, ThesisListActivity.class);
            goToThesisList.putExtra("student_name", studentName);
            startActivity(goToThesisList);
            finish();
        });
    }

    @Override
    public void onFailure(String error) {
        this.runOnUiThread(() -> {
            progressDialog.dismiss();
            Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
        });
    }

    private String getNameString(String name) {
        try {
            JSONObject jsonObject =  new JSONObject(name);
            return (String) jsonObject.get("full_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

}
